import React, { Component } from "react";
// import Aux from "../../hoc/Aux";
import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Burger/BuildControls/BuildControls';
import Modal from '../../Components/UI/Modal/modal';
import OrderSummary from '../../Components/OrderSummary/OrderSummary'


const INGREDIENT_PRICE = {
  meat: 20,
  potato: 10,
  salad: 5,
  cheese:15
}
class BurgerBuilder extends Component {
  
  // constructor(props)
  // {
  //   super(props);
  //   this.state = { };
  //  }

  state = {
    ingredient:{
      meat: 0,
      potato: 0,
      salad: 0,
      cheese:0
    },
    totalprice: 50,
    purchaseable: false,
    ordered:false
    }

   placeOrder(ingredient){ 
  
     const sum = Object.keys(ingredient)
      .map(igkey => {
        return ingredient[igkey];
      }).reduce((sum, el) => {
        return sum + el;
      }, 0);
     
     this.setState({ purchaseable: sum > 0 });
  }

  addIngredientHandler = (type) => {
    const oldcount = this.state.ingredient[type];
    const updatecount = oldcount + 1;
    const updatedIngredient = {
      ...this.state.ingredient
    };
    updatedIngredient[type] = updatecount;
    const priceAddition = INGREDIENT_PRICE[type];
    const oldprice = this.state.totalprice;
    const newprice = oldprice + priceAddition;  

    this.setState({ totalprice: newprice, ingredient: updatedIngredient });
    this.placeOrder(updatedIngredient);

  }

  removeIngredientHandler=(type)=>
  {
    const oldcount = this.state.ingredient[type];
    if (oldcount <= 0)
    {
      return;
      }
    const updatecount = oldcount - 1;
    const updatedIngredient = {
      ...this.state.ingredient
    };
    updatedIngredient[type] = updatecount;
    const pricededuction = INGREDIENT_PRICE[type];
    const oldprice = this.state.totalprice;
    const newprice = oldprice -pricededuction;  

    this.setState({ totalprice: newprice, ingredient: updatedIngredient });
    this.placeOrder(updatedIngredient);
  }

  orderHandler = () => {
    this.setState({ ordered: true });
  }

  orderremoveHandler = () => {
    this.setState({ ordered: false });  
  }

  ordercheckoutHandler (){
    alert("You checked Out");
    
  }

  render() {
    return (
      <>
        <Modal show={this.state.ordered} modalremove={this.orderremoveHandler}>
          <OrderSummary
            ingredient={this.state.ingredient}
            price={this.state.totalprice}
            ordercancel={this.orderremoveHandler}
            ordercheckout={this.ordercheckoutHandler}
          />
        </Modal>
        <Burger ingredient={this.state.ingredient}/>
        <BuildControls
          ingredientadd={this.addIngredientHandler}
          ingredientdelete={this.removeIngredientHandler}
          purchaseable={this.state.purchaseable}
          ordered={this.orderHandler}
          price={this.state.totalprice}
        />

        
      </>
    );
  }
}

export default BurgerBuilder;
