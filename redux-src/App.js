import React, { Component } from "react";
import { connect } from "react-redux";
import { addName } from "./services/action";
import { bindActionCreators } from "redux";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      number: null
    };
  }
  show = () => {
    console.log(this.props.empData);
  };
  render() {
    const datahandle = () =>
      this.props.addName(this.state.name, this.state.number);
    

    return (
      <>
        <div className="App">
          <input
            type="text"
            placeholder="name"
            name="first"
            onChange={event => this.setState({ name: event.target.value })}
          />
          <input
            type="text"
            placeholder="number"
            name="second"
            onChange={event => this.setState({ number: event.target.value })}
          />
          <button onClick={datahandle}> Create </button>
          <button onClick={this.show}> Read </button>
          <button> Update </button>
          <button> Delete</button>
        </div>
        <table>
          <tr>
            <th>name</th>
            <th>number</th>
          </tr>
          {this.props.empData.map(item => (
            <tr>
              <td>{item.name}</td>
              <td>{item.number}</td>
            </tr>
          ))}
        </table>
      </>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addName }, dispatch);
}

const mapStateToProps = state => {
  
  return { empData: state };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
