import { ADD_DATA } from "./constant";

export const addName = (name, number) => {
  const action = {
    type: ADD_DATA,
    name,
    number,
    id: Math.random()
  };
  return action;
};
