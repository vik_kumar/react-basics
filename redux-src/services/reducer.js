
import { ADD_DATA } from "./constant";

const dataBind = action => {
  return { name: action.name, number: action.number, id: action.id };
};

export default function AppReducer(state = [], action) {
  switch (action.type) {
    case ADD_DATA:
      return [...state, dataBind(action)];
    default:
      return state;
  }
}
